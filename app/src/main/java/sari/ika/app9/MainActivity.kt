package sari.ika.app9

import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(){

    //Inisiasi Variabel
    var posLaguSkrg = 0
    var posVidSkrg = 0
    //var handler = Handler()
    lateinit var mediaController: android.widget.MediaController

    lateinit var db : SQLiteDatabase
    lateinit var adapter : ListAdapter


    override fun onStart() {
        super.onStart()
        showDataVideo()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        lsVid.setOnItemClickListener(itemClick)
        mediaController = MediaController(this)
        mediaController.setPrevNextListeners(nextVid, prevVid)
        mediaController.setAnchorView(videoView)
        videoView.setMediaController(mediaController)
//        videoSet(posVidSkrg)
        db = DBOpenHelper(this).writableDatabase
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c: Cursor = lsVid.adapter.getItem(position) as Cursor
        var id_video = c.getString(c.getColumnIndex("_id"))
        var id_cover = c.getInt(c.getColumnIndex("id_cover"))
        var judul = c.getString(c.getColumnIndex("video_title"))

        imV.setImageResource(id_cover)
        txJud.setText(judul)
        posVidSkrg = position
//        videoSet(posVidSkrg)

        videoView.setVideoURI(Uri.parse("android.resource://"+packageName+"/"+c.getInt(c.getColumnIndex("_id"))))
    }

    fun showDataVideo(){
        val cursor : Cursor = db.query("video", arrayOf("id_video as _id","id_cover","video_title"),null,null,null,null,"id_video asc")
        adapter = SimpleCursorAdapter(this,R.layout.item_data_video,cursor,
            arrayOf("_id","id_cover","video_title"), intArrayOf(R.id.txIdVid, R.id.txIdCvr, R.id.txJdlVid),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        lsVid.adapter = adapter
    }
//

    var nextVid = View.OnClickListener { v:View ->
        if(posVidSkrg<(lsVid.adapter.count-1)) posVidSkrg++
        else posVidSkrg = 0
//        videoSet(posVidSkrg)
//
        val c: Cursor = lsVid.adapter.getItem(posVidSkrg) as Cursor
        var id_video = c.getString(c.getColumnIndex("_id"))
        var id_cover = c.getInt(c.getColumnIndex("id_cover"))
        var judul = c.getString(c.getColumnIndex("video_title"))

        imV.setImageResource(id_cover)
        txJud.setText(judul)

        videoView.setVideoURI(Uri.parse("android.resource://"+packageName+"/"+c.getInt(c.getColumnIndex("_id"))))
    }

    var prevVid = View.OnClickListener { v:View ->
        if(posVidSkrg>0) posVidSkrg--
        else posVidSkrg = lsVid.adapter.count-1
//        videoSet(posVidSkrg)
//
        val c: Cursor = lsVid.adapter.getItem(posVidSkrg) as Cursor
        var id_video = c.getString(c.getColumnIndex("_id"))
        var id_cover = c.getInt(c.getColumnIndex("id_cover"))
        var judul = c.getString(c.getColumnIndex("video_title"))

        imV.setImageResource(id_cover)
        txJud.setText(judul)

        videoView.setVideoURI(Uri.parse("android.resource://"+packageName+"/"+c.getInt(c.getColumnIndex("_id"))))
    }


}
